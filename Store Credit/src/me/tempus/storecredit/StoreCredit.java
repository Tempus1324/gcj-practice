package me.tempus.storecredit;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class StoreCredit {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		
		final ArrayList<String> lines = new ArrayList<String>();
		
		BufferedReader reader;
		
		reader = new BufferedReader(new FileReader("A-small-practice.in"));
		
		String currentLine;
		final int cases = Integer.parseInt(reader.readLine());
		while((currentLine = reader.readLine()) != null){
			lines.add(currentLine);
		}
		
		reader.close();
		
		for(int n = 0; n < cases; n++){
			final int storeCredit = Integer.parseInt(lines.get((n *3)));
			
			final int[] storePrices = new int[Integer.parseInt(lines.get((n* 3) +1))];
			
			final String[] items = lines.get((n * 3) + 2).split(" ");
			int indexOne = -1;
			int indexTwo = -1;
			for(int i = 0; i < items.length; i++){
				final int lookingFor = storeCredit - Integer.parseInt(items[i]);
				for(int j = 0; j < items.length; j++){
					if(j != i){
						if(Integer.parseInt(items[j]) == lookingFor){
							indexTwo = j+1;
							break;
						}
					}
				}
				if(indexTwo != -1){
					indexOne = i+1;
					break;
				}
			}
			System.out.println("Case #" + (n+1) + " " + indexOne + " " +indexTwo);
		}
	}

}
