/**
 * 
 */
package me.tempus.reversewords;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Chris
 *
 */
public class ReverseWords {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		final ArrayList<String> fileLines = new ArrayList<String>();
		final int size;
		final String[] words;
		BufferedReader reader = null;
		
		try {
			reader = new BufferedReader(new FileReader("B-large-practice.in"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String currentLine;
		while((currentLine = reader.readLine()) != null){
			fileLines.add(currentLine);
		}
		
		size = Integer.parseInt(fileLines.get(0));
		
		words = new String[size];
		
		for(int i = 1; i < fileLines.size(); i++){
			words[i -1] = fileLines.get(i);
		}
		
		for(int i = 0; i < words.length; i++){
			String[] values = words[i].split(" ");
			StringBuilder reversedWord = new StringBuilder();
			reversedWord.append("Case #" + (i+1) + " ");
			final int length = values.length;
			for(int j = 0; j < length; j++){
				reversedWord.append(values[(length-1) - j] + " ");
			}
			System.out.println(reversedWord.toString());
		}
		
	}

}
